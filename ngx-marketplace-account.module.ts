import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgxMarketplaceAccountComponent } from './ngx-marketplace-account.component';
import { NgxMaterialModule } from '@4geit/ngx-material-module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgxMaterialModule,
  ],
  declarations: [
    NgxMarketplaceAccountComponent
  ],
  exports: [
    NgxMarketplaceAccountComponent
  ]
})
export class NgxMarketplaceAccountModule { }
