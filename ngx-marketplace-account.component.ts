import { Component, OnInit } from '@angular/core';
import { MdSnackBar } from '@angular/material';

import { NgxMarketplaceProductsService } from '@4geit/ngx-marketplace-products-service';

@Component({
  selector: 'ngx-marketplace-account',
  template: require('pug-loader!./ngx-marketplace-account.component.pug')(),
  styleUrls: ['./ngx-marketplace-account.component.scss']
})
export class NgxMarketplaceAccountComponent implements OnInit {

  orders = [
    {
      id: '1234-567',
      amount: 1234.45,
      payment: 'visa',
      date: '2017-07-01'
    }
  ];

  constructor(
    private snackBar: MdSnackBar,
    private productsService: NgxMarketplaceProductsService,
  ) { }

  ngOnInit() {
  }

}
