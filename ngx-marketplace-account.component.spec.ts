import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxMarketplaceAccountComponent } from './ngx-marketplace-account.component';

describe('marketplace-account', () => {
  let component: marketplace-account;
  let fixture: ComponentFixture<marketplace-account>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ marketplace-account ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(marketplace-account);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
